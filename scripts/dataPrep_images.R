# clear workspace
rm(list=ls())

# load libraries
library(jpeg)
library(doBy)
library(colorspace)

# load functions
source('scripts/functions/cultureData.R')

# load and plot image
par(mar=rep(0, 4))
image <- readJPEG('data/cultureImages/test.jpg')
plot(c(0, dim(image)[1]), c(0, dim(image)[2]), type='n', xlab='', ylab='')
rasterImage(image, 0, 0, dim(image)[1], dim(image)[2], interpolate=F)

# select four corners of image, then get data from image
img <- locator(4)
data <- culture.data(ncultures=2, npoints=8, plat.mm=90)


# load and plot second image
par(mar=rep(0, 4))
image <- readJPEG('data/cultureImages/test2.jpg')
plot(c(0, dim(image)[1]), c(0, dim(image)[2]), type='n', xlab='', ylab='')
rasterImage(image, 0, 0, dim(image)[1], dim(image)[2], interpolate=F)

# select four corners of image, then get data from image
img <- locator(4)
data1 <- culture.data(ncultures=1, npoints=4, plat.mm=90)


# load and plot tiff image (NOT SURE WORKING PROPERLY FOR TIFF IMAGES)
par(mar=rep(0, 4))
image <- readJPEG('data/cultureImages/dan.tiff')
plot(c(0, dim(image)[1]), c(0, dim(image)[2]), type='n', xlab='', ylab='')
rasterImage(image, 0, 0, dim(image)[1], dim(image)[2], interpolate=F)

# select four corners of image, then get data from image
img <- locator(4)
data1 <- culture.data(ncultures=1, npoints=4, plat.mm=90)

